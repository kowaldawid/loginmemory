﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace logging.Models
{
    public class Authorization
    {
        public int identicalElements { get; set; }
        public int elements { get; set; }
        public int round { get; set; }
        //public List<string> dane { get; set; }
    }

    public class Configuration
    {
        public int identicalElements { get; set; }
        public int elements { get; set; }
        public int round { get; set; }
        public string nameFile { get; set; }
    }
}