﻿using logging.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Web.Http;

namespace logging.Controllers
{
    public class LoginAuthorizationController : ApiController
    {
        public void Get(string url)
        {
            var config = Helpers.Json.Configuration;
            try
            {
                Image.SaveImage(config.nameFile + ".png", url, ImageFormat.Png, "Content");
            }
            catch (ExternalException)
            {
                //logger.Fatal
            }
            catch (ArgumentNullException)
            {
                //logger.Fatal
            }
        }

        public HttpResponseMessage Post(int identicalElements, int elements, int round, string check)
        {
            var config = Helpers.Json.Configuration;
            if (identicalElements == config.identicalElements && elements == config.elements && round == config.round &&
                Image.CheckImage(check.Substring(check.LastIndexOf('/') + 1), config.nameFile + ".png"))
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK };

            return new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized };
        }
    }
}