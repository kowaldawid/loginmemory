﻿using logging.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;

namespace logging.Controllers
{
    public class AuthorizationController : ApiController
    {
        public async System.Threading.Tasks.Task<HttpResponseMessage> GetAsync(int elements, int identicalElements)
        {
            var hash = new string[elements];
            var images = new string[elements / identicalElements];
            var random = new Random();
            var sha = SHA1.Create();
            var request = new Request();

            var google = await request.GetAsync("q=clipart&filter=1&imgColorType=color&imgSize=small&imgType=clipart&start=" + random.Next(1, 89) + "&num=" + (elements / identicalElements) + "&searchType=image");
            if (google == null)
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.BadRequest };

            google.items[random.Next(0, (elements / identicalElements))].link = "http://" + System.Web.HttpContext.Current.Request.Url.Authority + "/Content/my.png";

            foreach (var img in google.items)
            {
                var r = random.Next(1, 89);
                images[google.items.IndexOf(img)] = "../Resources/" + r + ".png";
                try
                {
                    Image.SaveImage(r + ".png", img.link, ImageFormat.Png);
                }
                catch (ExternalException)
                {
                    //logger.Fatal
                }
                catch (ArgumentNullException)
                {
                    //logger.Fatal
                }
            }

            for (int i = 0; i < elements; i++)
            {
                 var hex = HexStringFromBytes(sha.ComputeHash(
                    Encoding.UTF8.GetBytes(random.Next().ToString())));
                if(hash.FirstOrDefault(s => s == hex) == null)
                    hash[i] = hex;
                else
                    --i;           
            }

            return new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(JsonConvert.SerializeObject(new {
                    hash,
                    images
                }),
                System.Text.Encoding.UTF8,
                "application/json")
            };
        }

        public async System.Threading.Tasks.Task<HttpResponseMessage> PostAsync()
        {
            var data = JsonConvert.DeserializeObject<Models.Authorization>(
                await Request.Content.ReadAsStringAsync());

            string script = "$.when(cheat).done(function(e){!function(){function n(){return void 0===n.counter&&(n.counter=0),++n.counter}var t=[],i=[],o=!0,r=0,a=[];repetitionCheck=function(e,n,t){for(var i=0,o=!0,r=0;r<n.length;r++)n[r]===t&&++i>e&&(o=!1);return o};for(var c=0;c<" + data.elements + ";c++)!function(n){do{var r=Math.floor(Math.random()*" + data.elements + "),a=repetitionCheck(0,t,r)}while(!1===a);t[n]=r;do{var c=Math.floor(Math.random()*(" + data.elements + "/" + data.identicalElements + ")),a=repetitionCheck(" + data.identicalElements + "-1,i,c)}while(!1===a);i[n]=c;var l=e.hash[r],s=e.images[c];$('#'+l).on('click',function(){o&&(o=!1,$(this).css({'background-image':'url(\"'+s+'\")','background-color':'#FFF','background-size':'75px 75px'}).prop('disabled',!0),setTimeout(function(){trafficCheck(s)},200))})}(c);trafficCheck=function(e){for(var t=!0,i=0;i<r;i++)a[i]!==e&&(t=!1);if(!0===t)if(r===" + data.identicalElements + "-1){var c=n();c===" + data.round + "&&(trafficCheck.round=e),$('#memory').find('.row').children('button:disabled').removeAttr('id').css('background-image','url(../Content/b.png)').css('visibility','hidden'),r=0,a=[],c===" + data.elements + "/" + data.identicalElements + "&&($('#loader-wrapper').show(200),$('#memory').empty(500),$.post('../api/LoginAuthorization?identicalElements='+" + data.identicalElements + "+'&elements='+elements+'&round='+" + data.round + "+'&check='+trafficCheck.round,function(){setTimeout(function(){$('#memory').append('<div class=\"alert alert-success\" style=\"text-align: center;z-index: 1001;position: relative;top: 20px;\">Zalogowano!!!</div>')},500)}).fail(function(){setTimeout(function(){$('#memory').append('<div class=\"alert alert-danger\" style=\"text-align: center;z-index: 1001;position: relative;top: 20px;\">Niepoprawne logowanie!!!</div>')},200)}))}else a[r]=e,r++;else $('#memory').find('.row').children().css('background-image','url(../Content/b.png)').prop('disabled',!1),r=0,a=[];o=!0};for(var l=0;l<" + data.elements + ";l++)e.hash[l]='hacking, keep trying  :)',e.images[l]='hacking, keep trying  :)',t[l]='hacking, keep trying  :)',i[l]='hacking, keep trying  :)'}()});";

            return new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(script,
                System.Text.Encoding.UTF8,
                "text/javascript")
            };
        }

        public static string HexStringFromBytes(byte[] bytes)
        {
            var sb = new StringBuilder();
            foreach (byte b in bytes)
            {
                var hex = b.ToString("x2");
                sb.Append(hex);
            }
            return sb.ToString();
        }
    }


}