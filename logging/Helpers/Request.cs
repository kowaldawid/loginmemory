﻿using logging.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;

namespace logging.Helpers
{
    public class Request
    {
        private HttpClient client;
        private readonly string baseUri;
        private readonly string requestUri;

        private const string BASE_URI = "https://www.googleapis.com/customsearch";
        private const string API_LEVEL = "v1";

        private static string KEY = WebConfigurationManager.AppSettings["api:SecretKey"];
        private static string CX = WebConfigurationManager.AppSettings["api:SearchEngine"];

        internal Request(string baseUri, string requestUri)
        {
            this.baseUri = baseUri;
            this.requestUri = requestUri;
        }

        public Request() : this(BASE_URI + "/", API_LEVEL + "?key=" + KEY + "&cx=" + CX + "&")
        {
        }

        public async Task SendAsync(string data)
        {
            using (client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri(baseUri);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage response = await client.PostAsJsonAsync(requestUri, data);
                    if (response.IsSuccessStatusCode)
                    {
                        //logger.Info
                    }
                    else
                    {
                        //logger.Fatal
                    }
                }
                catch (Exception e)
                {
                    //logger.Fatal
                }
            }
        }

        public async Task<Google> GetAsync(string path)
        {
            using (client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri(baseUri);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage response = await client.GetAsync(requestUri + path);

                    if (response.IsSuccessStatusCode)
                    {
                        return await response.Content.ReadAsAsync<Google>();
                    }
                    else
                    {
                        await response.Content.ReadAsStringAsync();
                        //logger.Error  
                    }

                }
                catch (Exception e)
                {
                    //logger.Error
                }

                return null;
            }
        }
    }
}