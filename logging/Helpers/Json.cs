﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using Config = logging.Models.Configuration;

namespace logging.Helpers
{
    public class Json
    {
        public static Config Configuration
        {
            get
            {
                using (StreamReader r = new StreamReader(HttpContext.Current.Server.MapPath("/") + "register_config.json"))
                {
                    string json = r.ReadToEnd();
                    Config items = JsonConvert.DeserializeObject<Config>(json);
                    return items;
                }
            }
        }
    }
}