﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Web;

namespace logging.Helpers
{
    public class Image
    {
        public static void SaveImage(string filename, string imageUrl, ImageFormat format, string folder = "Resources")
        {
            using (var client = new WebClient())
            using (var stream = client.OpenRead(imageUrl))
            using (var bitmap = new Bitmap(stream))
            {
                if (bitmap != null)
                {
                    var resizedImage = new Bitmap(bitmap, 75, 75);
                    resizedImage.Save(HttpContext.Current.Server.MapPath("/"+ folder + "/") + filename, format);
                }
            }
        }

        public static bool CheckImage(string filename, string filenameUser)
        {
            byte[] m1, m2;

            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(HttpContext.Current.Server.MapPath("/Resources/") + filename))
                {
                    m1 = md5.ComputeHash(stream);
                }
            }

            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(HttpContext.Current.Server.MapPath("/Content/") + filenameUser))
                {
                    m2 = md5.ComputeHash(stream);
                }
            }

            if (m1.SequenceEqual(m2))
                return true;

            return false;
        }



    }
}